# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.3.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-pagecollection-to-output-html/compare/v1.3.1...v1.3.2) (2024-03-06)

### [1.3.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-pagecollection-to-output-html/compare/v1.3.0...v1.3.1) (2024-03-06)

## [1.3.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-pagecollection-to-output-html/compare/v1.2.0...v1.3.0) (2024-03-06)


### Features

* use kimi-baseworker v3.7.0 ([e272d56](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-pagecollection-to-output-html/commit/e272d56a774c0985d8af5deec4b8ca1da9915a41))

## [1.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-pagecollection-to-output-html/compare/v1.1.0...v1.2.0) (2024-03-06)


### Features

* use latest baseworker and logger ([01fc3ff](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-pagecollection-to-output-html/commit/01fc3ff1cc1f6827cdf479149d66363a745458c9))

## 1.1.0 (2023-09-21)


### Features

* add component to logs ([3f0cc53](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-pagecollection-to-output-html/commit/3f0cc53cfd19a705de08bd1eba75d393c9b1b111))
* add ECS logging ([96b46b7](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-pagecollection-to-output-html/commit/96b46b7652ff79cb20c61b499982f202afa53a20))
* add index.ts ([51e4f7f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-pagecollection-to-output-html/commit/51e4f7f3050669235bfa6aef3bd29f4251d52687))
* update to latest baseworker ([a419c26](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-pagecollection-to-output-html/commit/a419c26a03bbf3ffb34cecf8a26be141f5a9ffe3))
* use base worker ([628bdd1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-pagecollection-to-output-html/commit/628bdd16bb7c1c923af986b2a4b83356eedf1a7d))
* use latest baseworker ([1b05f6c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-pagecollection-to-output-html/commit/1b05f6c3c4c3ed1ae6e5ec767bc01afc53bb4002))
* use logging format json ([c09c38c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-pagecollection-to-output-html/commit/c09c38c285edf9947380f49aecd0de738f6afc34))
