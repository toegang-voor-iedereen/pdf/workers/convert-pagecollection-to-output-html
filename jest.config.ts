import { JestConfigWithTsJest } from 'ts-jest'

/*
 * For a detailed explanation regarding each configuration property and type check, visit:
 * https://jestjs.io/docs/configuration
 */
const config: JestConfigWithTsJest = {
    // Automatically clear mock calls, instances, contexts and results before every test
    clearMocks: true,

    // Indicates whether the coverage information should be collected while executing the test
    collectCoverage: true,

    // An array of glob patterns indicating a set of files for which coverage information should be collected
    collectCoverageFrom: [
        'src/**/*.ts',
        '!src/**/*.interface.ts',
        'src/**/*.js',
        '!**/node_modules/**',
    ],

    // The directory where Jest should output its coverage files
    coverageDirectory: 'coverage',

    // Indicates which provider should be used to instrument code for coverage
    coverageProvider: 'v8',

    // A list of reporter names that Jest uses when writing coverage reports
    coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],

    // A preset that is used as a base for Jest's configuration
    preset: 'ts-jest/presets/default-esm',
    resolver: 'ts-jest-resolver',

    reporters: ['default', 'jest-junit'],

    // The test environment that will be used for testing
    testEnvironment: 'node',

    // The regexp pattern or array of patterns that Jest uses to detect test files
    testRegex: ['.spec.ts$'],
    // The paths to modules that run some code to configure or set up the testing environment before each test
    setupFiles: ['<rootDir>/__test__/setup.ts'],
    transform: {
        '^.+\\.(ts|tsx)?$': ['ts-jest', { useESM: true }],
    },
    testPathIgnorePatterns: ['./dist'],
}
export default config
