#
# Builder stage.
# This state compile our TypeScript to get the JavaScript code
#
FROM node:18-alpine AS builder

WORKDIR /

COPY package*.json ./
COPY tsconfig*.json ./
COPY esbuild.config.mjs ./
COPY ./src ./src
RUN npm ci --quiet && npm run build

#
# Build Production stage.
# This state compile get back the JavaScript code from builder stage
# It will also install the production package only
#
FROM node:18-alpine as production-builder

WORKDIR /
ENV NODE_ENV=production

COPY package*.json ./
RUN npm ci --quiet --only=production

FROM node:18-alpine as production

WORKDIR /
ENV NODE_ENV=production

COPY package*.json ./
## We just need the dist folder and node_modules to execute the command
COPY --from=builder ./dist ./dist
COPY --from=production-builder ./node_modules ./node_modules
RUN npm install -g npm

## Start
CMD ["npm", "run", "start"]