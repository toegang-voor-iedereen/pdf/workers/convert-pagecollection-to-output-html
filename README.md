# Convert Pagecollection to output HTML

This application figures out the type of a file through looking at the files mimetype.

## Kubernetes
In the deployment directory you can find a Helm chart that sets up this application.

You can read more about Helm on their [website](https://helm.sh/)

### Local development

To run it on your local machine:

1. make sure you have [Docker Desktop](https://www.docker.com/products/docker-desktop/) installed 

2. [Enable kubernetes](https://docs.docker.com/desktop/kubernetes/) in Docker Desktop (This provides you with `kubectl` out of the box)

    - If you already have kubectl, it probably doesn't point to the docker-desktop environment. To change context do the following: 
    - `kubectl config get-contexts` to list the available contexts.
    - `kubectl config use-context docker-desktop` to set the context to docker-desktop

3. Install [Helm](https://helm.sh/)

4. run `docker build -t convert-pagecollection-to-output-html .` 

5. run `helm install convert-pagecollection-to-output-html ./deployment` 

This should spin up all resources which can then be inspected in Docker Desktop. 

To uninstall, run: `helm uninstall convert-pagecollection-to-output-html`.

## Conventional commits

This project uses standard version and commitizen. An interesting article about this approach can be read [here](https://blog.logrocket.com/automatically-generate-and-release-a-changelog-with-node-js/). `npm run release` will automatically up the version number in all relevant files (package-json, package.lock etc) based on the features and fixes and tag the release.

If you want to check out what the CHANGELOG.md will look like, run `npm install` and then run `npm run release -- --dry-run`, which will output the changelog to your terminal instead of actually tagging the release and saving the changelog.

There's also a command that walks you through creating a great commit message, it's `npm run commit`, and it will ask some questions about the things you are committing, and then creates a valid message for you. It's great for getting to know conventional commits. 

### Example commits

Commit message with just type and description:

```
feat: some cool feature without bugs
```

Commit message with type, scope, and description:

```
fix(parser): no bugs, no really
```

Commit message with BREAKING CHANGE:

```
refactor(parser): reworked all the things

BREAKING CHANGE: wow, so much changed!
```

If you want to read more about Conventional Commits, read more [here](https://www.conventionalcommits.org/)!