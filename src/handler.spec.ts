import { WorkerMessageHandlerInput } from '@toegang-voor-iedereen/kimi-baseworker'
import { Client } from 'minio'
import { handler } from './handler.js'
import { logger } from '@toegang-voor-iedereen/typescript-logger'

describe('handler', () => {
    const job: WorkerMessageHandlerInput = {
        logger,
        job: {
            recordId: 'some-record-id',
            bucketName: 'some-bucket',
            filename: 'some-file.pdf',
            attributes: {
                pages: '2',
                '1.text': 'This is page one.',
                '2.text': 'This is page two.',
            },
        },
        jobId: 'some-job-id',
    }
    it('should compose page contents into an html file', async () => {
        const putObject = jest.spyOn(Client.prototype, 'putObject').mockImplementation(async () => {
            return {
                etag: 'some-etag',
                versionId: 'some-version-id',
            }
        })
        await expect(handler(job)).resolves.toEqual({
            confidence: 100,
            fileResult: {
                bucketName: 'output',
                bucketPath: 'some-record-id/some-job-id/index.html',
                fileExtension: 'html',
            },
            success: true,
        })

        expect(putObject).toHaveBeenCalledWith(
            'output',
            'some-record-id/some-job-id/index.html',
            expect.any(String),
            { 'Content-Type': 'text/html' }
        )

        expect(putObject.mock.calls[0][2]).toMatchInlineSnapshot(`
            "<html>
            <head>
            <meta charset="UTF-8">
            </head>
            <body>
            <p>This is page one.</p><p>This is page two.</p>
            </body>
            </html>"
        `)
    })
})
