import * as Sentry from '@sentry/node'
Sentry.init({
    dsn: process.env.SENTRY_DSN,
    tracesSampleRate: parseFloat(process.env.SENTRY_TRACES_SAMPLE_RATE ?? '1.0'),
})

import { sleep } from './utils/index.js'

import { Worker, WorkerConfig } from '@toegang-voor-iedereen/kimi-baseworker'
import { handler } from './handler.js'
import { logger as tslogger } from '@toegang-voor-iedereen/typescript-logger'

const name = 'worker-convert-pagecollection-to-output'
const logger = tslogger.child({
    component: name,
    workerVersion: process.env.npm_package_version ?? '0.0',
})

try {
    if (!process.env.EXCHANGE) {
        throw new Error('No exchange prefix defined')
    }

    const config: WorkerConfig = {
        name,
        amqpHost: process.env.AMQP_HOST ?? 'localhost',
        amqpPass: process.env.AMQP_PASS,
        amqpPort: process.env.AMQP_PORT ?? '5672',
        amqpProtocol: process.env.AMQP_PROTOCOL ?? 'amqp',
        amqpUser: process.env.AMQP_USER,
        exchangePrefix: process.env.EXCHANGE,
        workerInstanceName: process.env.HOSTNAME ?? '',
        prefetchLimit: 10,
    }

    const worker = new Worker({
        logger,
        messageHandler: handler,
        config,
    })

    await worker.start()
} catch (error) {
    if (error instanceof Error) {
        logger.fatal(error)
        await sleep(2000) // Make sure log transports have time to propagate
        await Sentry.flush(2000)
    }
    throw error
}
