import {
    FileWorkerHandlerResult,
    WorkerMessageHandler,
} from '@toegang-voor-iedereen/kimi-baseworker'
import { Client as MinioClient } from 'minio'

const MINIO_HOST = process.env.MINIO_HOST ?? 'localhost'
const MINIO_PORT = parseInt(process.env.MINIO_PORT ?? '9000')
const MINIO_ACCESS_KEY = process.env.MINIO_ACCESS_KEY ?? 'test'
const MINIO_SECRET_KEY = process.env.MINIO_SECRET_KEY ?? 'test'
const MINIO_USE_SSL: boolean = JSON.parse(process.env.MINIO_USE_SSL ?? 'false')

const minioClient = new MinioClient({
    endPoint: MINIO_HOST,
    port: MINIO_PORT,
    useSSL: MINIO_USE_SSL,
    accessKey: MINIO_ACCESS_KEY,
    secretKey: MINIO_SECRET_KEY,
})

export const handler: WorkerMessageHandler = async ({
    job,
    jobId,
}): Promise<FileWorkerHandlerResult> => {
    const pages = parseInt(job.attributes['pages'])
    let text = '<p>'
    for (let pageNumber = 1; pageNumber <= pages; pageNumber++) {
        if (text.endsWith('.')) {
            text += '\n\n'
        }
        text += job.attributes[`${pageNumber}.text`] ?? ''
    }
    text += '</p>'

    text = text.replaceAll('\n\n', '</p><p>').replaceAll('\n', '<br/>')

    const html = `<html>
<head>
<meta charset="UTF-8">
</head>
<body>
${text}
</body>
</html>`

    const bucket = 'output'
    const fileExtension = 'html'
    const bucketPath = `${job.recordId}/${jobId}/index.${fileExtension}`
    await minioClient.putObject(bucket, bucketPath, html, {
        'Content-Type': 'text/html',
    })

    return {
        success: true,
        fileResult: {
            bucketName: bucket,
            bucketPath: bucketPath,
            fileExtension: fileExtension,
        },
        confidence: 100,
    }
}
