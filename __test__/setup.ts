import { jest } from '@jest/globals'

global.jest = jest

// Always unset Sentry DSN
delete process.env.SENTRY_DSN
process.env.LOGGING_ENABLED = 'false'
